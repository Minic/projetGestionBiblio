<%@page import="com.ynov.model.technique.DuplicatedPseudoException"%>
<%@page import="com.ynov.model.technique.DuplicatedEmailException"%>
<%@page import="com.ynov.model.technique.ForbiddenException"%>
<%@page import="com.ynov.controleur.Manager"%>
<%@page import="com.ynov.utilitaire.Role"%>
<%@page import="com.ynov.service.UtilisateurPOJO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.ynov.utilitaire.StatutCompte"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Inscription</title>
    <link rel="stylesheet" href="css/inscription.css" />
	<link rel="stylesheet" href="css/footer.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>

<body>

  <div class="form-wrap">
		<div class="tabs">
			<h3 class="signup-tab"><a class="active" href="#signup-tab-content">Inscription</a></h3>
			<h3 class="login-tab"><a href="#login-tab-content">Connexion</a></h3>
		</div><!--.tabs-->
<%

String nom = request.getParameter("nom");
String prenom = request.getParameter("prenom");
String sexe = request.getParameter("sexe");
String email = request.getParameter("email");
String date = request.getParameter("dateDeNaissance");
String pseudo = request.getParameter("pseudo");
String mdp = request.getParameter("mdp");

if(session.getAttribute("user")!= null){
	response.sendRedirect("index.jsp");
}

if(nom != null && prenom != null && sexe != null && email != null && date != null && pseudo != null && mdp != null){
	Date dateDeNaissance = new Date();
	dateDeNaissance.setYear(Integer.parseInt(date.substring(0, 3)));
	dateDeNaissance.setMonth(Integer.parseInt(date.substring(5, 6)));
	dateDeNaissance.setDate(Integer.parseInt(date.substring(8, 9)));
	UtilisateurPOJO newUser = new UtilisateurPOJO(null, nom, prenom, sexe, null, email, dateDeNaissance, pseudo, mdp, StatutCompte.ACTIF, null, Role.USER);
	try{
		Manager.getInstance().ajouterUtilisateur(newUser);
		out.println("Utilisateur créé");
	}catch(DuplicatedEmailException e){
		out.println("Mail déjà utilisé");
	}catch(DuplicatedPseudoException e){
		out.println("Pseudo déjà utilisé");
	}
		
}

if(request.getParameter("connexion") != null && pseudo != null && mdp != null){
	if(!Manager.getInstance().connexion(pseudo, mdp)){
		out.println("La connexion a échoué");
	}else{
		session.setAttribute("user", Manager.getInstance().getUtilisateurConnecte());
		response.sendRedirect("index.jsp");
	}
}
%>
		<div class="tabs-content">
			<div id="signup-tab-content" class="active">
				<form class="signup-form" action="" method="post">
          <input type="text" class="input" id="nom" name="nom" autocomplete="off" placeholder="Nom">
          <input type="text" class="input" id="prenom" name="prenom" autocomplete="off" placeholder="Prénom">
          <p>Date de naissance</p>
          <input type="date" class="input" id="dateDeNaissance" name="dateDeNaissance" autocomplete="off">
         	<input type="text" class="input" id="pseudo" name="pseudo" autocomplete="off" placeholder="Pseudo">
			<input type="password" class="input" id="mdp" name="mdp" autocomplete="off" placeholder="Mot de passe">
			<input type="email" class="input" id="email" name="email" autocomplete="off" placeholder="Email">
			<input type="text" class="input" id="sexe" name="sexe" autocomplete="off" placeholder="Sexe">
			<input type="submit" class="button" value="Inscription">
				</form><!--.login-form-->
			</div><!--.signup-tab-content-->

			<div id="login-tab-content">
				<form class="login-form" action="" method="post">
					<input type="hidden" id="connexion" name="connexion" value="true"/>
					<input type="text" class="input" id="pseudo" name="pseudo" autocomplete="off" placeholder="Pseudo">
					<input type="password" class="input" id="mdp" name="mdp" autocomplete="off" placeholder="Mot de passe">
				
					<input type="submit" class="button" value="Connexion">
				</form><!--.login-form-->
				
			</div><!--.login-tab-content-->
		</div><!--.tabs-content-->
	</div><!--.form-wrap-->


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

    <script  src="js/inscription.js"></script>


<footer>
			<div>
				<div id="txtfooter">
						Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai - Thomas Palazetti
					</br>
						Projet JEE
				</div>
			</div>
		</footer>

</body>

</html>
