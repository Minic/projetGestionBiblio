<%@page import="com.ynov.controleur.Manager"%>
<%@page import="com.ynov.service.AuteurPOJO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Liste des Auteurs</title>
  
  
  
      <link rel="stylesheet" href="css/ConsulterLivre.css">
  <link rel="stylesheet" href="css/footer.css" />

  
</head>

<body>

	<div><a href="./index.jsp"><h1>Accueil</h1></a></div>
	
  <div class="filters">

  <!--<input type="text" id="id" placeholder="Id" />-->
  <input type="text" id="nom" placeholder="Nom" />
  <input type="text" id="prenom" placeholder="Prénom" />
  <button>Reset</button>
 
</div>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nom</th>
      <th>Prénom</th>
      <th>A propos</th>
      <th>Nationalité</th>
      <th>Modification</th>
    </tr>
  </thead>
  <tbody>
   <%
  List<AuteurPOJO> auteurs = Manager.getInstance().consultationAuteurs();
  for(AuteurPOJO auteur : auteurs){
	  out.println("<tr data-id=\"" + auteur.getId() + "\" data-nom=\""+auteur.getNom()+"\" data-prenom=\"" +auteur.getPrenom()+"\">");
		  out.println("<td>" + auteur.getId() +"</td>");
		  out.println("<td>" + auteur.getNom() +"</td>");
		  out.println("<td>" + auteur.getPrenom() +"</td>");
		  out.println("<td>" + auteur.getaProposDe() +"</td>");
		  out.println("<td>" + auteur.getNationalite() +"</td>");
		  
		  
		  	//out.println("<td><form method=\"POST\"><input name='id' value='"+auter.getId()+"' type='hidden'/><input name='emprunter' value='true' type='hidden'/><button type='submit'>Emprunter</button></form></td>");
		  	
		  	
	  out.println("</tr>");
  }
  %>
  </tbody>
</table>





    <a href="ConsulterLivre.jsp"> <button>Consulter les<br/> livres</button></a>
    <a href="ConsulterCategorie.jsp"> <button>Consulter les catégories</button></a>


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js'></script>
  <script  src="js/Filtre.js"></script>



<footer>
      <div>
        <div id="txtfooter">
            Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai - Thomas Palazetti
          </br>
            Projet JEE
        </div>
      </div>
    </footer>
</body>

</html>
