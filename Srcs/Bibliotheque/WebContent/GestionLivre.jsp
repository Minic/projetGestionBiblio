<%@page import="com.ynov.service.LivrePOJO"%>
<%@page import="com.ynov.service.CategoriePOJO"%>
<%@page import="com.ynov.controleur.Manager"%>
<%@page import="com.ynov.service.AuteurPOJO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>Liste des livres</title>



<link rel="stylesheet" href="css/ConsulterLivre.css">
<link rel="stylesheet" href="css/footer.css" />


</head>

<body>

	<div class="filters">

		<!--<input type="text" id="id" placeholder="Id" />-->
		<input type="text" id="titre" placeholder="Titre" /> <input
			type="text" id="autheur" placeholder="Autheur" />
		<button>Reset</button>

	</div>

	<div class="form-wrap">
		<div class="tabs">
			<h3 class="signup-tab">
				<a class="active" href="#signup-tab-content">Ajouter un livre</a>
			</h3>
		</div>

		<div class="tabs-content">
			<div id="signup-tab-content" class="active">
				<form class="signup-form" onsubmit="ajouterLivre(this); return false;" method="post">
					<input type="text" class="input" name="titre" autocomplete="off" placeholder="Titre" required="required"/>
					<select name="auteur" required="required">
						<%
							for(AuteurPOJO auteur : Manager.getInstance().consultationAuteurs()) {
								  out.println("<option value='"+auteur.getId()+"'>" + auteur.getNom() + " " + auteur.getPrenom() +"</option>");
							}
						%>
					</select>
					<input type="text" class="input" name="isbn" autocomplete="off" placeholder="ISBN" required="required"/>
					<input type="text" class="input" name="quantite" autocomplete="off" placeholder="Quantité" required="required"/>
					<input type="text" class="input" name="resume" autocomplete="off" placeholder="resume" />
					<input type="text" class="input" name="urlPhoto" autocomplete="off" placeholder="urlPhoto" />
					<select name="categorie">
						<%
							for(CategoriePOJO categorie : Manager.getInstance().consultationCategories()) {
								  out.println("<option value='"+categorie.getId()+"'>" + categorie.getNom() +"</option>");
							}
						%>
					</select>
					<input type="date" class="input" name="datePublication"autocomplete="off" placeholder="datePublication" required="required"/>
					<input type="submit" class="button" value="Ajout" />
				</form>
			</div>
		</div>
		
		<script type="text/javascript">
		function ajouterLivre(form){
			var json = formToJson(form);
			json.auteur = { id: json.auteur };
			json.categorie = { id: json.categorie };
			$.ajax({
				method:'post',
				data: JSON.stringify(json),
				url:'./api/livre',
				contentType: "application/json; charset=utf-8"
			}).then(function(){
				alert('Livre ajouté')
			}, function(err) {
				console.error(err);
				alert('erreur sur le livre (voir la console)');
			})
		}

		function supprimerLivre(form){
			var id = $('select',form).val();
			$.ajax({
				method:'delete',
				data: {idLivre:id},
				url:'./api/livre'
			}).then(function(){
				alert('Livre supprimé')
			}, function(err) {
				console.error(err);
				alert('erreur sur le livre (voir la console)');
			})
		}
			
			function formToJson(form) {//serialize data function
				
				formArray = $(form).serializeArray();
				
				var returnArray = {};
				for (var i = 0; i < formArray.length; i++){
					returnArray[formArray[i]['name']] = formArray[i]['value'];
				}
				return returnArray;
			}
		</script>
		
		<div>Supprimer</div>
		<form class="signup-form" onsubmit="supprimerLivre(this); return false;" method="post">
			<select name="id" required="required">
						<%
							for(LivrePOJO livre : Manager.getInstance().consultationLivres()) {
								  out.println("<option value='"+livre.getId()+"'>" + livre.getTitre() +"</option>");
							}
						%>
					</select>
					<input type="submit" class="button" value="Supprimer" />
				</form>



		<a href="AjoutLivre.html"><button>Ajouter</button></a> <a
			href="GestionMembre.html">
			<button>GÃ©rer les membres</button>
		</a> <a href="GestionCategorie.html">
			<button>GÃ©rer les catÃ©gories</button>
		</a>

		<script
			src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
		<script
			src='http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js'></script>
		<script src="js/Filtre.js"></script>



		<footer>
			<div>
				<div id="txtfooter">
					Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai
					- Thomas Palazetti </br> Projet JEE
				</div>
			</div>
		</footer>
</body>

</html>
