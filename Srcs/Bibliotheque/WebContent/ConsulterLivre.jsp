<%@page import="java.util.Vector"%>
<%@page import="com.ynov.service.LivrePOJO"%>
<%@page import="com.ynov.controleur.Manager"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Liste des livres</title>
  
  
  
      <link rel="stylesheet" href="css/ConsulterLivre.css">
  <link rel="stylesheet" href="css/footer.css" />

  
</head>

<body>
<%
if(session.getAttribute("user")== null){
	response.sendRedirect("index.jsp");
}
%>

	<div><a href="./index.jsp"><h1>Accueil</h1></a></div>
<%
Long idEmprunt = null;
try{
	idEmprunt = Long.parseLong(request.getParameter("id"));
}catch(NumberFormatException e){
}
out.println("<div style=\"margin-bottom:10px\">");
if(request.getParameter("emprunter") != null && idEmprunt != null ){
	try{
		Manager.getInstance().emprunterLivre(idEmprunt);
		out.println("Le livre a bien été emprunté");
	}catch(UnavailableException e){
		out.println("Le livre demandé n'es pas disponible");
	}
}else if(request.getParameter("emprunter") != null){
	out.println("Le format de l'id de l'emprunt n'est pas un entier :)");
}
out.println("</div>");
%>
  <div class="filters">

  <!--<input type="text" id="id" placeholder="Id" />-->
  <input type="text" id="titre" placeholder="Titre" />
  <input type="text" id="autheur" placeholder="Autheur" />
  <button>Reset</button>
 
</div>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Titre</th>
      <th>Categorie</th>
      <th>Résumé</th>
      <th>Quantité</th>
      <th>Auteur</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  <%
  List<LivrePOJO> livres = Manager.getInstance().consultationLivres();
  for(LivrePOJO livre : livres){
	  out.println("<tr data-id=\"" + livre.getId() + "\" data-titre=\""+livre.getTitre()+"\" data-auteur=\"" +livre.getAuteur().getNom()+" "+livre.getAuteur().getPrenom()+"\">");
		  out.println("<td>" + livre.getId() +"</td>");
		  out.println("<td>" + livre.getTitre() +"</td>");
		  out.println("<td>" + livre.getCategorie().getNom() +"</td>");
		  out.println("<td>" + livre.getResume() +"</td>");
		  out.println("<td>" + livre.getQuantite() +"</td>");
		  out.println("<td>" + livre.getAuteur().getNom()+" "+livre.getAuteur().getPrenom() +"</td>");
		  	out.println("<td><form method=\"POST\"><input name='id' value='"+livre.getId()+"' type='hidden'/><input name='emprunter' value='true' type='hidden'/><button type='submit'>Emprunter</button></form></td>");
	  out.println("</tr>");
  }
  %>
  
  </tbody>
</table>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js'></script>

  
    <a href="ConsulterAuteur.jsp"> <button>Consulter les auteurs</button></a>
    <a href="ConsulterCategorie.jsp"> <button>Consulter les catégories</button></a>

    <script  src="js/Filtre.js"></script>



<footer>
      <div>
        <div id="txtfooter">
            Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai - Thomas Palazetti
          </br>
            Projet JEE
        </div>
      </div>
    </footer>
</body>

</html>
