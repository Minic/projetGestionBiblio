package com.ynov.vue;

import java.util.Date;
import java.util.List;

import com.ynov.controleur.Manager;
import com.ynov.model.technique.DuplicatedEmailException;
import com.ynov.model.technique.DuplicatedPseudoException;
import com.ynov.model.technique.ForbiddenException;
import com.ynov.service.UtilisateurPOJO;
import com.ynov.utilitaire.Role;
import com.ynov.utilitaire.StatutCompte;

public class Biblio {

	public static void main(String[] args) {
		// 
		UtilisateurPOJO admin = new UtilisateurPOJO(null, "admin", "admin", "robot", null, "admin@admin.dev", new Date(), "admin", "admin", StatutCompte.ACTIF, null, Role.ADMIN);
		try {
			Manager.getInstance().ajouterUtilisateur(admin);
			System.out.println("OK");
		} catch (ForbiddenException e) {
			System.out.println("ERREEEUR");
			e.printStackTrace();
		} catch (DuplicatedEmailException e) {
			System.out.println("Mail double");
			e.printStackTrace();
		} catch (DuplicatedPseudoException e) {
			System.out.println("Pseudo double");
			e.printStackTrace();
		}
		
		if(Manager.getInstance().connexion("admin", "admin")) {
			System.out.println("connect�");
		}else {
			System.out.println("caca");
		}
		
		UtilisateurPOJO user = new UtilisateurPOJO(null, "Moi", "Nom", "enfant", null, "moi@local.dev", new Date(), "moi", "azerty", StatutCompte.ACTIF, null, Role.USER);
		try {
			Manager.getInstance().ajouterUtilisateur(user);
			System.out.println("OK");
		} catch (ForbiddenException e) {
				System.out.println("ERREEEUR");
				e.printStackTrace();
			} catch (DuplicatedEmailException e) {
				System.out.println("Mail double");
				e.printStackTrace();
			} catch (DuplicatedPseudoException e) {
				System.out.println("Pseudo double");
				e.printStackTrace();
			}
		
		afficherUtilisateurs();
		
		Manager.getInstance().deconnexion();
		
		afficherUtilisateurs();
		
		if(Manager.getInstance().connexion("moi", "azerty")) {
			System.out.println("connect�");
		}else {
			System.out.println("caca");
		}

		afficherUtilisateurs();
	}
	
	public static void afficherUtilisateurs() {
		try {
			List<UtilisateurPOJO> utilisateurs = Manager.getInstance().consultationUtilisateurs();
			System.out.println("Les Utilisateur ! ("+utilisateurs.size()+")");
			for (UtilisateurPOJO utilisateur : utilisateurs ) {
				System.out.println("Utilisateur:\n\tID:\t"+utilisateur.getId()+
						"\n\tPrenom:\t"+utilisateur.getPrenom()+
						"\n\tNom:\t"+utilisateur.getNom()+
						"\n\tPseudo:\t"+utilisateur.getPseudo()+
						"\n\tSexe:\t"+utilisateur.getSexe()+
						"\n\tMdp:\t"+utilisateur.getMdp()+
						"\n\tUrlPhoto:\t"+utilisateur.getUrlPhoto());
			}
		} catch (ForbiddenException e) {
			System.out.println("Je ne peux pas afficher les utilisateus");
			e.printStackTrace();
		}
	}
}
