package com.ynov.model.technique;

import com.ynov.model.metier.Auteur;
import com.ynov.service.AuteurPOJO;
import com.ynov.stockage.Dao;
import com.ynov.utilitaire.Conversion;

public class AuteurManager {
	
	private Auteur auteur;
	private AuteurPOJO sp;
	private Dao<AuteurPOJO> dao;
	private Long cle;

	/**
	 * Constructeur par defaut qui init une auteur manager
	 */
	public AuteurManager() {
		cle = -1L;
		auteur = null;
		sp = null;
		dao = null;
	}

	/**
	 * Constructeur parametrer o� l'on choisi l'index et la dao utiliser
	 * @param clef
	 * @param d
	 */
	public AuteurManager(Long clef, Dao<AuteurPOJO> d) {
		cle = clef;
		dao = d;
		init();
	}

	/**
	 * Constructeur parametrer o� l'on choisi la auteurpojo et la dao utiliser
	 * @param p
	 * @param d
	 */
	public AuteurManager(AuteurPOJO p, Dao<AuteurPOJO> d) {
		cle = p.getId();
		sp = p;
		dao = d;
		auteur = Conversion.pojoToAuteur(p);
	}

	/**
	 * cette methode va permettre d'initialiser une auteur � partir de sa auteurpojo grace a le methode de conversion
	 * @see Conversion
	 */
	private void init() {
		sp = dao.lire(cle);
		auteur = Conversion.pojoToAuteur(sp);
	}

	/**
	 * retourne les infos de la auteur manager
	 */
	@Override
	public String toString() {
		return String.join(" ", "id:", ("" + cle), auteur.toString());
	}

	/**
	 * 
	 * @return la auteur pojo
	 */
	public AuteurPOJO getPOJO() {
		return sp;
	}

	
	/**
	 * Cette methode va permettre de supprimer une auteur de la bdd
	 */
	public void supprimer() {
		if(auteur != null){
			dao.effacer(cle);
			auteur = null;
		}
	}

	/**
	 * Cette methode va ajouter une auteur � la bdd
	 * @param sp
	 */
	public void ajouter(AuteurPOJO sp) {
		dao.inserer(sp);
	}

	/**
	 * Cette methode va metre a jour l'utilisateur � la bdd
	 * @param sp
	 */
	public void update(AuteurPOJO sp) {
		if(auteur != null) {
			dao.update(auteur.getId(), sp);
			this.sp = sp;
		}
	}

}
