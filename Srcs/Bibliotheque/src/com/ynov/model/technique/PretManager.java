package com.ynov.model.technique;

import com.ynov.model.metier.Pret;
import com.ynov.service.PretPOJO;
import com.ynov.stockage.Dao;
import com.ynov.utilitaire.Conversion;

public class PretManager {
	private Pret pret;
	private PretPOJO sp;
	private Dao<PretPOJO> dao;
	private Long cle;

	/**
	 * Constructeur par defaut qui init une pret manager
	 */
	public PretManager() {
		cle = -1L;
		pret = null;
		sp = null;
		this.dao = null;
	}
	
	/**
	 * Constructeur parametrer o� l'on choisi l'index et la this.dao utiliser
	 * @param clef
	 * @param dao
	 */
	public PretManager(Dao<PretPOJO> dao) {
		this.dao = dao;
	}

	/**
	 * Constructeur parametrer o� l'on choisi l'index et la this.dao utiliser
	 * @param clef
	 * @param dao
	 */
	public PretManager(Long clef, Dao<PretPOJO> dao) {
		cle = clef;
		this.dao = dao;
		init();
	}

	/**
	 * Constructeur parametrer o� l'on choisi la pretpojo et la this.dao utiliser
	 * @param p
	 * @param dao
	 */
	public PretManager(PretPOJO p, Dao<PretPOJO> dao) {
		cle = p.getId();
		sp = p;
		this.dao = dao;
		pret = Conversion.pojoToPret(p);
	}

	/**
	 * cette methode va permettre dao'initialiser une pret � partir de sa pretpojo grace a le methode de conversion
	 * @see Conversion
	 */
	private void init() {
		sp = this.dao.lire(cle);
		pret = Conversion.pojoToPret(sp);
	}

	/**
	 * retourne les infos de la pret manager
	 */
	@Override
	public String toString() {
		return String.join(" ", "id:", ("" + cle), pret.toString());
	}

	/**
	 * 
	 * @return la pret pojo
	 */
	public PretPOJO getPOJO() {
		return sp;
	}

	
	/**
	 * Cette methode va permettre de supprimer une pret de la bdd
	 */
	public void supprimer() {
		if(pret != null){
			this.dao.effacer(cle);
			pret = null;
		}
	}

	/**
	 * Cette methode va ajouter une pret � la bdd
	 * @param sp
	 */
	public void ajouter(PretPOJO sp) {
		this.dao.inserer(sp);
	}

	/**
	 * Cette methode va metre a jour l'utilisateur � la bdd
	 * @param sp
	 */
	public void update(PretPOJO sp) {
		if(pret != null) {
			dao.update(pret.getId(), sp);
			this.sp = sp;
		}
	}
}
