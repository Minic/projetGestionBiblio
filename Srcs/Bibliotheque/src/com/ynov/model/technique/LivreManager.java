package com.ynov.model.technique;

import com.ynov.model.metier.Livre;
import com.ynov.service.LivrePOJO;
import com.ynov.stockage.Dao;
import com.ynov.utilitaire.Conversion;

public class LivreManager {
	private Livre livre;
	private LivrePOJO sp;
	private Dao<LivrePOJO> dao;
	private Long cle;

	/**
	 * Constructeur par defaut qui init une livre manager
	 */
	public LivreManager() {
		cle = -1L;
		livre = null;
		sp = null;
		this.dao = null;
	}

	/**
	 * Constructeur parametrer o� l'on choisi l'index et la this.dao utiliser
	 * @param clef
	 * @param dao
	 */
	public LivreManager(Long clef, Dao<LivrePOJO> dao) {
		cle = clef;
		this.dao = dao;
		init();
	}

	/**
	 * Constructeur parametrer o� l'on choisi la livrepojo et la this.dao utiliser
	 * @param p
	 * @param dao
	 */
	public LivreManager(LivrePOJO p, Dao<LivrePOJO> dao) {
		cle = p.getId();
		sp = p;
		this.dao = dao;
		livre = Conversion.pojoToLivre(p);
	}

	/**
	 * cette methode va permettre dao'initialiser une livre � partir de sa livrepojo grace a le methode de conversion
	 * @see Conversion
	 */
	private void init() {
		sp = this.dao.lire(cle);
		livre = Conversion.pojoToLivre(sp);
	}

	/**
	 * retourne les infos de la livre manager
	 */
	@Override
	public String toString() {
		return String.join(" ", "id:", ("" + cle), livre.toString());
	}

	/**
	 * 
	 * @return la livre pojo
	 */
	public LivrePOJO getPOJO() {
		return sp;
	}

	
	/**
	 * Cette methode va permettre de supprimer une livre de la bdd
	 */
	public void supprimer() {
		if(livre != null){
			this.dao.effacer(cle);
			livre = null;
		}
	}

	/**
	 * Cette methode va ajouter une livre � la bdd
	 * @param sp
	 */
	public void ajouter(LivrePOJO sp) {
		this.dao.inserer(sp);
	}

	/**
	 * Cette methode va metre a jour l'utilisateur � la bdd
	 * @param sp
	 */
	public void update(LivrePOJO sp) {
		if(livre != null) {
			dao.update(livre.getId(), sp);
			this.sp = sp;
		}
	}
}
