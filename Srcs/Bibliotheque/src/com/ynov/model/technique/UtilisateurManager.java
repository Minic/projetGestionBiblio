package com.ynov.model.technique;

import com.ynov.model.metier.Utilisateur;
import com.ynov.service.UtilisateurPOJO;
import com.ynov.stockage.Dao;
import com.ynov.utilitaire.Conversion;

public class UtilisateurManager {
	private Utilisateur utilisateur;
	private UtilisateurPOJO sp;
	private Dao<UtilisateurPOJO> dao;
	private Long cle;

	/**
	 * Constructeur par defaut qui init une utilisateur manager
	 */
	public UtilisateurManager() {
		cle = -1L;
		utilisateur = null;
		sp = null;
		dao = null;
	}

	/**
	 * Constructeur parametrer o� l'on choisi l'index et la dao utiliser
	 * @param clef
	 * @param d
	 */
	public UtilisateurManager(Long clef, Dao<UtilisateurPOJO> d) {
		cle = clef;
		dao = d;
		init();
	}

	/**
	 * Constructeur parametrer o� l'on choisi la utilisateurpojo et la dao utiliser
	 * @param p
	 * @param d
	 */
	public UtilisateurManager(UtilisateurPOJO p, Dao<UtilisateurPOJO> d) {
		cle = p.getId();
		sp = p;
		dao = d;
		utilisateur = Conversion.pojoToUtilisateur(p);
	}

	/**
	 * cette methode va permettre d'initialiser une utilisateur � partir de sa utilisateurpojo grace a le methode de conversion
	 * @see Conversion
	 */
	private void init() {
		sp = dao.lire(cle);
		utilisateur = Conversion.pojoToUtilisateur(sp);
	}

	/**
	 * retourne les infos de la utilisateur manager
	 */
	@Override
	public String toString() {
		return String.join(" ", "id:", ("" + cle), utilisateur.toString());
	}

	/**
	 * 
	 * @return la utilisateur pojo
	 */
	public UtilisateurPOJO getPOJO() {
		return sp;
	}

	
	/**
	 * Cette methode va permettre de supprimer une utilisateur de la bdd
	 */
	public void supprimer() {
		if(utilisateur != null){
			dao.effacer(cle);
			utilisateur = null;
		}
	}

	/**
	 * Cette methode va ajouter une utilisateur � la bdd
	 * @param sp
	 */
	public void ajouter(UtilisateurPOJO sp) {
		dao.inserer(sp);
	}

	/**
	 * Cette methode va metre a jour l'utilisateur � la bdd
	 * @param sp
	 */
	public void update(UtilisateurPOJO sp) {
		if(utilisateur != null) {
			dao.update(utilisateur.getId(), sp);
			this.sp = sp;
		}
	}
}
