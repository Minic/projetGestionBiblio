package com.ynov.service;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.ynov.utilitaire.TypeAuteur;

@Entity
@Table(name ="auteur")
@XmlRootElement(name="auteur")
public class AuteurPOJO extends PersonnePOJO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String aProposDe;
	private String nationalite;
	private TypeAuteur type;
	
	public AuteurPOJO() {
		super();
	}

	public AuteurPOJO(Long id, String nom, String prenom, String sexe, String urlPhoto, String email, Date dateDeNaissance,
			String aProposDe, String nationalite, TypeAuteur type) {
		super(id, nom, prenom, sexe, urlPhoto, email, dateDeNaissance);
		this.aProposDe = aProposDe;
		this.nationalite = nationalite;
		this.type = type;
	}

	public String getaProposDe() {
		return aProposDe;
	}

	public void setaProposDe(String aProposDe) {
		this.aProposDe = aProposDe;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public TypeAuteur getType() {
		return type;
	}

	public void setType(TypeAuteur type) {
		this.type = type;
	}

}
