package com.ynov.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: PretPOJO
 *
 */
@Entity
@Table(name="pret")
@XmlRootElement(name="pret")
public class PretPOJO implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private LivrePOJO livre;
	private UtilisateurPOJO utilisateur;
	@Temporal(TemporalType.DATE)
	private Date dateDePret;
	private int duree;


	public PretPOJO() {
		super();
	}
	public PretPOJO(Long id, LivrePOJO livre, UtilisateurPOJO utilisateur, Date dateDePret, int duree) {
		super();
		this.id = id;
		this.livre = livre;
		this.utilisateur = utilisateur;
		this.dateDePret = dateDePret;
		this.duree = duree;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LivrePOJO getLivre() {
		return livre;
	}

	public void setLivre(LivrePOJO livre) {
		this.livre = livre;
	}

	public UtilisateurPOJO getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurPOJO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Date getDateDePret() {
		return dateDePret;
	}

	public void setDateDePret(Date dateDePret) {
		this.dateDePret = dateDePret;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}
   
}
