package com.ynov.stockage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.ynov.service.CategoriePOJO;

public class DaoCategorie implements Dao<CategoriePOJO> {

	@PersistenceContext(unitName="Bibliotheque")
	private EntityManager em;
	
	public DaoCategorie() {
		//construire le CONTEXTE JPA
		em = Persistence.createEntityManagerFactory("Bibliotheque").createEntityManager();
	}
	
	@Override
	public List<CategoriePOJO> lireTous() {
		return em.createQuery("SELECT c FROM CategoriePOJO c",CategoriePOJO.class).getResultList();
	}

	@Override
	public CategoriePOJO lire(Long cle) {
		return em.find(CategoriePOJO.class, cle);
	}

	@Override
	public void inserer(CategoriePOJO elt) {
		em.getTransaction().begin();
		em.persist(elt);
		em.getTransaction().commit();
	}

	@Override
	public void update(Long index, CategoriePOJO obj) {
		update(obj);
	}
	
	public void update(CategoriePOJO obj) {
		em.getTransaction().begin();
		em.merge((CategoriePOJO)obj);
		em.getTransaction().commit();
	}

	@Override
	public void effacer(Long cle) {
		CategoriePOJO tmp = null;
		em.getTransaction().begin();
		tmp = em.find(CategoriePOJO.class, cle);
		em.remove(tmp);
		em.getTransaction().commit();
	}

}
