package com.ynov.stockage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.ynov.service.PretPOJO;

public class DaoPret implements Dao<PretPOJO> {

	@PersistenceContext(unitName="Bibliotheque")
	private EntityManager em;
	
	public DaoPret() {
		//construire le CONTEXTE JPA
		em = Persistence.createEntityManagerFactory("Bibliotheque").createEntityManager();
	}
	
	@Override
	public List<PretPOJO> lireTous() {
		return em.createQuery("SELECT p FROM PretPOJO p",PretPOJO.class).getResultList();
	}

	@Override
	public PretPOJO lire(Long cle) {
		return em.find(PretPOJO.class, cle);
	}

	@Override
	public void inserer(PretPOJO elt) {
		em.getTransaction().begin();
		em.persist(elt);
		em.getTransaction().commit();
	}

	@Override
	public void update(Long index, PretPOJO obj) {
		update(obj);
	}
	
	public void update(PretPOJO obj) {
		em.getTransaction().begin();
		em.merge((PretPOJO)obj);
		em.getTransaction().commit();
	}

	@Override
	public void effacer(Long cle) {
		PretPOJO tmp = null;
		em.getTransaction().begin();
		tmp = em.find(PretPOJO.class, cle);
		em.remove(tmp);
		em.getTransaction().commit();
	}

}
