package com.ynov.stockage;

import com.ynov.service.AuteurPOJO;
import com.ynov.service.CategoriePOJO;
import com.ynov.service.LivrePOJO;
import com.ynov.service.PretPOJO;
import com.ynov.service.UtilisateurPOJO;

/**
 * 
 *  G�rer le Singleton Dao
 *
 */
public class DaoFactory {

    /**
     * Instance de la factory
     */
    private static DaoFactory instance = null;
    
    private DaoFactory() {}
    
    /**
     * @return si la factory n'existe pas la cr�e puis la retourne
     */
    public static DaoFactory getInstance() {
        if (instance == null)
        {
            instance = new DaoFactory();
        }
        return instance;
    }

    /**
     * @return une nouvelle DaoJPA
     */
    public Dao<AuteurPOJO> getDaoAuteur() {
        return new DaoAuteur();
    }

    /**
     * @return une nouvelle DaoJPA
     */
    public Dao<CategoriePOJO> getDaoCategorie() {
        return new DaoCategorie();
    }

    /**
     * @return une nouvelle DaoJPA
     */
    public Dao<LivrePOJO> getDaoLivre() {
        return new DaoLivre();
    }

    /**
     * @return une nouvelle DaoJPA
     */
    public Dao<PretPOJO> getDaoPret() {
        return new DaoPret();
    }

    /**
     * @return une nouvelle DaoJPA
     */
    public Dao<UtilisateurPOJO> getDaoUtilisateur() {
        return new DaoUtilisateur();
    }
    
}