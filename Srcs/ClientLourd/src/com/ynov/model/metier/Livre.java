package com.ynov.model.metier;

import java.util.Date;

public class Livre {
	
	private Long id;
	private String titre;
	private String resume;
	private int quantite;
	private String isbn;
	private String urlPhoto;
	private Date datePublication;
	private Auteur auteur;
	private Categorie categorie;

	public Livre() {
		
	}
	
	public Livre(Long id, String titre, String resume, int quantite, String isbn, String urlPhoto, Date datePublication,
			Auteur auteur, Categorie categorie) {
		super();
		this.id = id;
		this.titre = titre;
		this.resume = resume;
		this.quantite = quantite;
		this.isbn = isbn;
		this.urlPhoto = urlPhoto;
		this.datePublication = datePublication;
		this.auteur = auteur;
		this.categorie = categorie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getUrlPhoto() {
		return urlPhoto;
	}

	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}

	public Date getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public Auteur getAuteur() {
		return auteur;
	}

	public void setAuteur(Auteur auteur) {
		this.auteur = auteur;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

}
