package com.ynov.model.metier;

import java.util.Date;

import com.ynov.utilitaire.TypeAuteur;

public class Auteur extends Personne {

	private String aProposDe;
	private String nationalite;
	private TypeAuteur type;
	
	public Auteur() {
	}

	public Auteur(Long id, String nom, String prenom, String sexe, String urlPhoto, String email, Date dateDeNaissance,
			String aProposDe, String nationalite, TypeAuteur type) {
		super(id, nom, prenom, sexe, urlPhoto, email, dateDeNaissance);
		this.aProposDe = aProposDe;
		this.nationalite = nationalite;
		this.type = type;
	}

	public String getaProposDe() {
		return aProposDe;
	}

	public void setaProposDe(String aProposDe) {
		this.aProposDe = aProposDe;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public TypeAuteur getType() {
		return type;
	}

	public void setType(TypeAuteur type) {
		this.type = type;
	}

}
